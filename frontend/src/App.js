import React from 'react';
import logo from './logo.svg';
import './App.css';
import Router from './components/Router';

function App() {
  return (
    <div className="App">
      <div className="App-body">
        <Router/>
      </div>
    </div>
  );
}

export default App;
