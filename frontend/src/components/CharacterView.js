import React from 'react';
import {fetchCharacterById} from '../api/characters';
export default class CharacterView extends React.Component {

  constructor(props){
    super(props);
    this.state = {
        character: null
      };
  }

  componentDidMount() {
    console.log(this.props.match.params.id);
    fetchCharacterById(this.props.match.params.id).then((character) => {
      this.setState({ character: character})
    }).catch(err => {
      console.error("unexpected error")
    });
  }

  render() {
    return (
      <div>
      {this.state.character == null ? (
        <h4>Cargando ....</h4>
      ):(
        <div>
        <img
          src={this.state.character.image}
          alt="new"
          />
          <h2>{this.state.character.name}</h2>
          <h4>{this.state.character.house}</h4>
          <h4>{this.state.character.crank}</h4>
          <h4>{this.state.character.slug}</h4>
          <h4>{this.state.character.titles}</h4>
          <h4>{this.state.character.books}</h4>
        </div>
      )}
      </div>
    );
  }
}
