import React from 'react';

export default class CharacterFilter extends React.Component {
  constructor(props){
    super(props);
    this.state = {value: ''};
  }

  _handleChangeValue(value){
    this.setState({value: value});
    // if callback defined
    if (this.props.onValueChanged)
      this.props.onValueChanged(value);
  }

  render() {
    return (
      <label>
          Name:
          <input type="text"
            value={this.state.value}
            onChange={(event) => this._handleChangeValue(event.target.value)} />
      </label>
    );
  }
}
