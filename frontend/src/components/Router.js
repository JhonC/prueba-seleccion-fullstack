import React from 'react';

import { Route, Link, HashRouter , Switch} from "react-router-dom";

import CharacterList from "./CharacterList";
import CharacterView from "./CharacterView";

export default class  Router extends React.Component {

  render() {
    return (
      <HashRouter>
        <div>
          <h1>GOT</h1>
          <Link className="App-link"  to="/"> HOME</Link>

          <Switch>
            <Route path="/character/:id" component={CharacterView}/>
            <Route path="/" component={CharacterList}/>
          </Switch>
        </div>
      </HashRouter>
    );
  }
}
