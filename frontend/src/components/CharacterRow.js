import React from 'react';

export default class CharacterRow extends React.Component {
  constructor(props){
    super(props)
  }

  render() {
    return (
      <h2>{this.props.character.name}, {this.props.character.house}</h2>
    );
  }
}
