import React from 'react';

import CharacterRow from './CharacterRow';
import CharacterFilter from './CharacterFilter';
import {Link} from "react-router-dom";

// load rest api
import {fetchCharacters} from '../api/characters';

export default class CharacterList extends React.Component {
  constructor(props){
    super(props);
    this.state = {
        characters: [],
        visibleCharacters: [],
        page: 0,
      };
  }

  componentDidMount() {
    this.loadCharacters(0);
  }

  loadCharacters(page){
    fetchCharacters(page).then((characters) => {
      this.setState({ characters: characters, visibleCharacters: characters})
    }).catch(err => {
      console.error("unexpected error");
    });
  }

  _filterByName(prefix) {
    if(prefix === ''){
      this.setState({ visibleCharacters: this.state.characters});
      return;
    }

    let filtered = this.state.visibleCharacters.filter(
          (aCharacter) =>
            aCharacter.name.toLowerCase().startsWith(prefix.toLowerCase())
              || aCharacter.house.toLowerCase().startsWith(prefix.toLowerCase())
          )
    this.setState({ visibleCharacters: filtered})
  }

  _changePage(delta) {
    const newPage = Math.max(this.state.page + delta, 0);
    console.log(newPage);
    this.setState({page: newPage});
    this.loadCharacters(newPage);
  }

  render() {
    return (
      <div>
        <div className="filterContainer">
          <CharacterFilter onValueChanged={(prefix) => this._filterByName(prefix)}/>
        </div>

        <div className="listContainer">
        {
          this.state.visibleCharacters.map((aCharacter)=>
            <Link className="App-link" key={aCharacter.id.toString()} to={"/character/" + aCharacter.id} ><CharacterRow character={aCharacter}/></Link>
          )
        }
        </div>
        <div>
          <a onClick={() => this._changePage(-1)}>Prev Page</a> | Page: {this.state.page} | <a onClick={() => this._changePage(1)}>Next Page</a>
        </div>
      </div>
    );
  }
}
