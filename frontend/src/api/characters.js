export const  fetchCharacters = async (page) => {
  // return [{"id":10,"name":"Aegon Frey (son of Aenys)","image":"undefined","gender":"male","slug":"Aegon_Frey_(son_of_Aenys)","crank":"undefined","books":"A Clash of Kings,A Storm of Swords,A Feast for Crows,A Dance with Dragons","titles":"","createdAt":"2020-08-18T22:40:35.000Z"},{"id":11,"name":"Abelar Hightower","image":"undefined","gender":"male","slug":"Abelar_Hightower","crank":"undefined","books":"The Hedge Knight","titles":"Ser","createdAt":"2020-08-18T22:54:21.000Z"},{"id":12,"name":"Addam","image":"undefined","gender":"null","slug":"Addam","crank":"undefined","books":"","titles":"","createdAt":"2020-08-18T22:54:21.000Z"}];
  const url = 'http://localhost:3000/characters/limit/10/page/' + page;
  return fetch(url, {
      method: "GET",
      headers: {
        'Content-Type': 'application/json'
      }
      })
      .then(res => res.json())
}

export const  fetchCharacterById = async (id: number) => {
  // return {"id":10,"name":"Aegon Frey (son of Aenys)","image":"undefined","gender":"male","slug":"Aegon_Frey_(son_of_Aenys)","crank":"undefined","books":"A Clash of Kings,A Storm of Swords,A Feast for Crows,A Dance with Dragons","titles":"","createdAt":"2020-08-18T22:40:35.000Z"};
  const url = 'http://localhost:3000/characters/' + id;
  return fetch(url, {
      method: "GET",
      headers: {
        'Content-Type': 'application/json'
      }
      })
      .then(res => res.json())
}
