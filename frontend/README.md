## Configuration
This a project developed on React entirely. The project was configured using the command `npx create-react-app`. You can run this project by executing the command `npm start`.
Unfortunately I'm not experienced configuring this type of projects, therefore it will throw an exception that will ask to change the port in which this app is deployed. And by pressing "yes" (accepting) it will run in port 3001.

Lastly you will see the project running at `localhost:3001`.

## Final notes
I did not have the time to remove all waste `npx` creates for a dummy project. I included two folders `api` and `components`. Folder `components` attempts to hold all React components. And Folder `api` manage requests to the backend. Lastly I would like to add that some improvements can be made on the code, specially on `CharactersList` component. The time was a big factor.
