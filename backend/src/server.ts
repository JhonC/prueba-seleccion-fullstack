import express from 'express';
import * as bodyParser from 'body-parser';
import cors from 'cors';


// import rest-services
import {CharactersRoute} from "./endpoints/CharactersRoute";

// Create a new express app instance
const app: express.Application = express();


// parse application/json
app.use(bodyParser.json())
app.use(cors())

// add main rest api
app.get('/', function (req, res) {
  res.send('Hello World!');
});
// register route/rest-api typehead
(new CharactersRoute()).register(app);

// run server
app.listen(3000, function () {
  console.log('You are awesome!, its working on port 3000!');
});
