import * as express from "express";
import request from "request";
import CharacterService from '../persistance/CharacterService'

export class CharactersRoute {
  private service: CharacterService

  public constructor(){
    this.service = new CharacterService()
  }

  public register(app: express.Application){
    /**
    * GET all characters paged
    *
    **/
    app.get( "/characters", (req: any, res) => {
      this.service.findAll(
        (err:any)=>{ // error
          res.status(500).json({'error': err})
        }, (result:any) => { // success
          res.status(200).json(result)
        }
      )
    });

    /**
    * GET all characters paged
    *
    **/
    app.get( "/characters/limit/:limit/page/:page", (req: any, res) => {
      this.service.findAllPagged(req.params.limit, req.params.page,
        (err:any)=>{ // error
          res.status(500).json({'error': err})
        }, (result:any) => { // success
          res.status(200).json(result)
        }
      )
    });

    /**
    * GET character by id
    *
    **/
    app.get( "/characters/:id", (req: any, res) => {
      this.service.findById( req.params.id,
        (err:any)=>{ // error
          res.status(500).json({'error': err})
        }, (result:any) => { // success
          res.status(200).json(result)
        }
      )
    });

    /**
    * GET character by id
    * Load characters from service
    **/
    app.get( "/load", (req: any, res) => {
      const _this = this
      request('https://api.got.show/api/book/characters', function (error, response, body) {
          if (!error && response.statusCode == 200) {
            const characters = JSON.parse(body);
            _this.service.insertAll(characters,
              (err:any)=>{ // error
                res.status(500).json({'error': err})
              }, () =>{ // success
                res.status(200).json({'success': 'capo!'})
              }
            )
          }
      })
    });
  }
}
