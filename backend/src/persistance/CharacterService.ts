import DBConnection from './DBConnection'
import { Character, CharacterProvider } from './Character'

export default class CharacterService {

    private db: DBConnection

    constructor(){
        this.db = new DBConnection
    }

    public findById(id: number, error: Function, success: Function) {
        let sql: string = CharacterProvider.findById(id)
        this.db.executeQuery(sql, error, (result: any)=> {
            success(result[0])
        })
    }

    public findAll(error: Function, success: Function) {
        let sql: string = CharacterProvider.findAll()
        this.db.executeQuery(sql, error, (result: any)=> {
            success(result)
        })
    }

    public findAllPagged(number:number, page: number, error: Function, success: Function) {
        let sql: string = CharacterProvider.findAllPagged(number, page)
        this.db.executeQuery(sql, error, (result: any)=> {
            success(result)
        })
    }

    public insert(character: Character, error: Function, success: Function) {
        let sql: string = CharacterProvider.insert(character)
        this.db.executeQuery(sql, error, (result: any)=> {
            success({id: result.insertId})
        })
    }

    public insertAll(characters: Array<Character>, error: Function, success: Function) {
        let sql: string = CharacterProvider.insertAll(characters)
        this.db.executeQuery(sql, error, (result: any)=> {
            success({id: result.insertId})
        })
    }
}
