export interface Character {
    id?: number
    name?: string
    image?: string
    slug?: string
    gender?: string
    crank?: string
    house?: string
    rank?: string
    books?: string
    titles?: string
}

export abstract class CharacterProvider {

    public static table:string = "character_table"

    public static deleteById(id: number) : string {
        return `DELETE FROM ${CharacterProvider.table} WHERE id = ${id};`
    }

    public static deleteAll(id: number) : string {
        return `TRUNCATE TABLE ${CharacterProvider.table} WHERE id = ${id};`
    }

    public static insert(character: Character) : string {
        return `INSERT INTO ${CharacterProvider.table} (name, house, image, slug, gender, crank, books, titles)
          VALUES ("${character.name}","${character.house}","${character.image}","${character.slug}","${character.gender}","${character.rank}","${character.books}","${character.titles}");`
    }

    public static insertAll(characters: Array<Character>) : string {
        const sql = `INSERT INTO ${CharacterProvider.table} (name, house, image, slug, gender, crank, books, titles) VALUES `;

        const sqlValues: Array<string> = characters.map((char) => {
          return `("${char.name}","${char.house}","${char.image}","${char.slug}","${char.gender}","${char.rank}","${char.books}","${char.titles}")`
        })

        return sql + sqlValues.join(`, `) + ";"

    }

    public static findById(id: number) : string {
        return `SELECT * FROM ${CharacterProvider.table} WHERE id = ${id};`
    }
    public static findAll() : string {
        return `SELECT * FROM ${CharacterProvider.table};`
    }

    public static findAllPagged(number: number, page: number) : string {
        return `SELECT *
          FROM ${CharacterProvider.table}
          LIMIT ${number} OFFSET ${number * page};`
    }

}
