DROP TABLE IF EXISTS character_table;
CREATE TABLE character_table (
    id INT(100) NOT NULL AUTO_INCREMENT,
    name TEXT(200),
    image TEXT(300),
    gender TEXT(10),
    slug TEXT(200),
    house TEXT(100),
    crank TEXT(200),
    books TEXT(200),
    titles TEXT(200),
    createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY(id));
