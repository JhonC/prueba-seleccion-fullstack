import * as mysql from 'mysql'
import config from '../config'
import { ConnectionConfig, Pool, Connection, PoolConfig } from 'mysql'

/**
 * Configure Mysql connection
 */
export default class DBConnection {

    private pool: Pool

    constructor() {
        this.pool = this.createPoolConnection()
    }

    private getDbPort() : number{
      let dbPort: number = parseInt(process.env.DB_PORT == undefined ? '3306': process.env.DB_PORT);
      return dbPort;
    }

    public createPoolConnection(): Pool {

        let connConf: PoolConfig = {
                    host: config.db.host,
                    port: this.getDbPort(),
                    user: config.db.user,
                    password: config.db.password,
                    database: config.db.database,
                    connectionLimit: 10
                }

        return mysql.createPool(connConf)
    }

    public createConnection(): mysql.Connection {
        let connConf: ConnectionConfig = {
                    host: config.db.host,
                    port: this.getDbPort(),
                    user: config.db.user,
                    password: config.db.password,
                    database: config.db.database,
                    multipleStatements:true
                }

        return mysql.createConnection(connConf)
    }

    /**
     *
     * @param sqlInstruction
     * @param errorCallback
     * @param successCallback
     */
    public executeQuery(sqlInstruction: string, errorCallback: Function, successCallback: Function, nested=false) : void {
        console.log(sqlInstruction);
        var _this = this
        this.pool.getConnection(function(err, connection) {
          if (err) {
            console.log(err);
            errorCallback(err);
            return;
          }
          // Use the connection
          connection.query({sql:sqlInstruction, nestTables: nested}, function (err2, results, fields) {
            // done with the connection.
            connection.release();
            // Handle error after the release.
            if (err2) {
              console.log(err2);
              errorCallback(err2);
              return;
            }
            successCallback(results)
          });
        });
    }

    /**
     *
     * @param sqlInstruction
     * @param errorCallback
     * @param callback
     */
    public executeSqlFile(sqlInstruction: string, errorCallback: Function, callback: Function) : void {
        let connection: Connection = this.createConnection()
        connection.connect((err: object) => {
            if (err) {
                errorCallback(err);
                return
            }

            connection.query(sqlInstruction, (err: object, result: object) => {
                if (err) {
                    errorCallback(err);
                    return
                }

                console.log(sqlInstruction, result)

                callback(result)
            })
        })
    }


}
