import * as fs from 'fs'
import DBConnection from './persistance/DBConnection'

let file = 'src/persistance/create.sql'

fs.readFile(file, 'utf8', function (err,data) {

    if (err) {
        console.log(err)
        return
    }

    let db: DBConnection = new DBConnection()

    console.log("Running create.sql")
    console.log(data)

    db.executeSqlFile(data,
        (result: object)=> {
            console.error(result)
            process.exit()
        },
        (result: object)=> {
            console.log(result)
            process.exit()
        })

})
