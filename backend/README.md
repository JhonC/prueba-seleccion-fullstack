## Configuration

This project runs using Docker Composer, Nodejs Express, Typescript and Mysql.
The `docker-compose.yml` file contains a list of configurations for Myslql, plus all server configurations.
You can find the list of configurations that the backend uses in `src/config.ts`.

Run `docker-compose up` to prepare & install and also run the project. It will compile the typescript and run the server at port 3000.
If you run the application for the first time, you have to also execute the following command to create the database.

```
docker-compose run webb npm run db_init
```

## Endpoints

Endpoint `GET localhost:3000/character/:id`, gets the a character by id.
Endpoint `GET localhost:3000/character`, gets all characters (no pagination).
Endpoint `GET localhost:3000/characters/limit/:limit/page/:page`, gets all characters by pagination and limit as parameters. This is a design decision of how to implement paggination.
Endpoint `GET localhost:3000/load` its an special endpoint to load characters from an external service `https://api.got.show/api/book/characters`. Note that this is just a design decision behaviour. I prefer to implement it in this way to interact only by API rest services.

## Final notes
Folder `src/endpoints` aims to hold only routes, although for the time I included much of the behaviour there. Whilst folder `src/persistance` was created to hold all persisitance behaviour, the mysql file is not supposed to be there, but I see no harm in moving it.
